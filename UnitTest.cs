using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects.Tests
{
    [TestClass()]
    public class TrainTests
    {
        [TestMethod()]
        public void isValidTrainIDTest()
        {
            // Arrange
            string trainId = "1A25";
           Train aTrain = new Train(trainId, "12:00", "Edinburgh(Waverley)", "London(Kings Cross)", "12/12/2000","Express", "True", "None","False");

            // Act
            bool result = aTrain.isValidTrainID(trainId);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void isValidTrainIDTest1()
        {
            // Arrange
            string trainId = "CA25";
            Train aTrain = new Train(trainId, "12:00", "Edinburgh(Waverley)", "London(Kings Cross)", "12/12/2000", "Express", "True", "None", "False");

            // Act
            bool result = aTrain.isValidTrainID(trainId);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void isValidDestinationTest()
        {
            // Arrange
            string departure = "Edinburgh(Waverley)";
            string destination = "London(Kings Cross)";
            Train aTrain = new Train("1A25", "12:00", departure, destination, "12/12/2000", "Express", "True", "None", "False");

            // Act
            bool result = aTrain.isValidDestination(destination,departure);

            // Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void isValidDestinationTest2()
        {
            // Arrange
            string departure = "London(Kings Cross)";
            string destination = "London(Kings Cross)";
            Train aTrain = new Train("1A25", "12:00", departure, destination, "12/12/2000", "Express", "True", "None", "False");

            // Act
            bool result = aTrain.isValidDestination(destination, departure);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void isValidDepartureTimeTest()
        {
            // Arrange
            string time = "12:30";
            Train aTrain = new Train("1A25", "12:00", "Edinburgh(Waverley)", "London(Kings Cross)", "12/12/2000", "Express", "True", "None", "False");

            // Act
            bool result = aTrain.isValidDepartureTime(time);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void isValidDepartureDayTest()
        {
            // Arrange
            string date = "12/10/2018";
            Train aTrain = new Train("1A25", "12:00", "Edinburgh(Waverley)", "London(Kings Cross)", date, "Express", "True", "None", "False");

            // Act
            bool result = aTrain.isValidDepartureDay(date);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void isValidTrainTypeTestExpress()
        {
            // Arrange
            string typeTrain = "Express";
            string intermediate = "None";
            string time = "12:30";
            string sleeperBerth = "False";
            Train aTrain = new Train("1A25", time, "Edinburgh(Waverley)", "London(Kings Cross)", "11/11/2018", typeTrain, "True", intermediate, sleeperBerth);

            // Act
            bool result = aTrain.isValidTypeTrain(typeTrain,intermediate,time, sleeperBerth);

            // Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void isValidTrainTypeTestStopping()
        {
            // Arrange
            string typeTrain = "Stopping";
            string intermediate = "York";
            string time = "12:30";
            string sleeperBerth = "False";
            Train aTrain = new Train("1A25", time, "Edinburgh(Waverley)", "London(Kings Cross)", "11/11/2018", typeTrain, "True", intermediate, sleeperBerth);

            // Act
            bool result = aTrain.isValidTypeTrain(typeTrain, intermediate, time, sleeperBerth);

            // Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void isValidTrainTypeTestSleeper()
        {
            // Arrange
            string typeTrain = "Sleeper";
            string intermediate = "York";
            string time = "21:30";
            string sleeperBerth = "True";
            Train aTrain = new Train("1A25", time, "Edinburgh(Waverley)", "London(Kings Cross)", "11/11/2018", typeTrain, "True", intermediate, sleeperBerth);

            // Act
            bool result = aTrain.isValidTypeTrain(typeTrain, intermediate, time, sleeperBerth);

            // Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void isValidSlepperBerthTest()
        {
            // Arrange
            string typeTrain = "Sleeper";
            string intermediate = "York";
            string time = "21:30";
            string sleeperBerth = "True";
            Train aTrain = new Train("1A25", time, "Edinburgh(Waverley)", "London(Kings Cross)", "11/11/2018", typeTrain, "True", intermediate, sleeperBerth);

            // Act
            bool result = aTrain.isSleepBerth(sleeperBerth, time, typeTrain);

            // Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void isValidSlepperBerthTest1()
        {
            // Arrange
            string typeTrain = "Sleeper";
            string intermediate = "York";
            string time = "11:30";
            string sleeperBerth = "True";
            Train aTrain = new Train("1A25", time, "Edinburgh(Waverley)", "London(Kings Cross)", "11/11/2018", typeTrain, "True", intermediate, sleeperBerth);

            // Act
            bool result = aTrain.isSleepBerth(sleeperBerth, time, typeTrain);

            // Assert
            Assert.IsFalse(result);
        }
        [TestMethod()]
        public void isValidSlepperBerthTest2()
        {
            // Arrange
            string typeTrain = "Sleeper";
            string intermediate = "York";
            string time = "21:30";
            string sleeperBerth = "False";
            Train aTrain = new Train("1A25", time, "Edinburgh(Waverley)", "London(Kings Cross)", "11/11/2018", typeTrain, "True", intermediate, sleeperBerth);

            // Act
            bool result = aTrain.isSleepBerth(sleeperBerth, time, typeTrain);

            // Assert
            Assert.IsTrue(result);
        }
    }
}